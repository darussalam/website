<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends AN_Apricot{

	public function __construct(){
		parent::__construct();
		
	}

	function index(){


		//ambil banner depan
		$this->load->library("banner_depan");

		$data=$this->public_data;
		$data["informasi"]["title"]=$this->title->home();
		$data["informasi"]["current_page"]="home";
		$data["informasi"]["uniqueid"]="home-page";

		$data["informasi"]["og-title"]=$data["informasi"]["title"];
		


		$data["banner_depan"]=$this->banner_depan->banner;
		$data["artikel_headline"]=$this->artikel->artikel_headline($this->system_info["max_headline_artikel"]);


	
		$this->load->view($this->tema."/header",$data);
		$this->load->view($this->tema."/home",$data);
		$this->load->view($this->tema."/footer",$data);

	}



    function login($x=''){
        if($this->login){
            redirect('home');
        }
        $data['status']=$x;
        $this->load->view("home",$data);
    }


	function proseslogin(){
        if($this->input->post()){
            $user=filterquote($this->input->post("username",TRUE),"all");
            $pass=md5($this->input->post("password"));

            $cari=$this->db->get_where("user",array("name_user"=>$user,"password_user"=>$pass,"status_user"=>"Y","terhapus"=>"N"));

            if($cari->num_rows()<1){
                redirect("home");
            }
            else{
                $row=$cari->row();
                $data_sessi=array('login'=>true,
                            'id_user'=>$row->id_user,
                            'name_user'=>$row->name_user,
                            'password_user'=>$row->password_user,
                            'level_user'=>$row->level_user);
                $this->session->set_userdata($data_sessi);
                redirect("admin");
            }

        }
        else{
            show_404();
        }
     }

}