<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//jika bukan halaman Home
if($informasi["current_page"]!='home'){

echo "<div class='col-md-4 right-wripper'>";

echo "<div class='right-side'>";

echo "<div class='right-side-konten' style='margin-bottom:50px;'>";
echo "<form method='post' action='".baseURL("form_visitors/search_article")."' class='form-group has-feedback'> 

  <input type='text' name='keyword' class='form-control search-form' placeholder='Cari Berita' />

</form>";
echo "</div>";

echo "<div class='right-side-konten'>";
echo "<h4><span>Berita</span></h4>";

foreach ($artikel_populer as $artikel) {
  
echo "<a href='".artikel_url($artikel['id'],$artikel['slug'])."'><div class='populer-artikel-right-box media'>";
echo "<div class='img-box media-left'>
 <img src='".img_artikel_url($artikel['foto'],true)."' alt='$artikel[judul]'/>
 </div>";

 echo "<div class='media-body konten-body'>";
 echo "<h5>$artikel[judul]</h5>";
 echo "</div>";

echo "</div></a>";


}

echo "</div>";


echo "<div class='right-side-konten'>";
echo "</div>";

echo "</div>"; //.right-side

echo "</div>"; //.col-md-4



  echo "</div>"; //tutup class .row
  echo "</div>"; //tutup class .container
  echo "</div>"; //tutup id #main-konten

}

?>



<div id='footer'>

  <div class="col-md-2">
  </div>
  <div class="col-md-1" style="padding-top: 10px">
    <img src="../an-component/media/filemanager-thumbs/location.png" style="width: 90px;height: 90px;
    position: relative; box-shadow: 1px 1px 50px #000; border-radius: 100%">
  </div>
  <div class="col-md-3">
    <h4>Jalan Raya Pantura (By Pass)</h4>Eretan Kulon, Kandanghaur<br><b>Indramayu</b> 45254
  </div>
  <div class="col-md-3" style="text-align: right;">
    0853 5389 9979<br>smadarussalamkdh@yahoo.com<br>smpdarussalamkdh@yahoo.com<br><h3 style="color: white">023 4505 140</h3>
  </div>
  <div class="col-md-1" style="padding-top: 10px">
    <img src="../an-component/media/filemanager-thumbs/contact.png" style="width: 90px;height: 90px;
    position: relative; box-shadow: 1px 1px 50px #000; border-radius: 100%">
  </div>
  <div class="col-md-2">
  </div>

</div>



</main>


<!--<script type="text/javascript" src="https://code.jquery.com/jquery-1.12.2.min.js"></script>-->
<script type="text/javascript" src="<?php echo assets_url('js/jquery-1.11.1.min.js'); ?>"></script>

<!--<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>-->
<script type="text/javascript" src="<?php echo assets_url('bootstrap/js/bootstrap.min.js'); ?>"></script>


<script type="text/javascript" src="<?php echo assets_url("js/jquery.singlePageNav.min.js") ?>"></script>

<!--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.js"></script>-->
<script type="text/javascript" src="<?php echo assets_url("fancybox/source/jquery.fancybox.js") ?>"></script>
<script type="text/javascript" src="<?php echo assets_url("fancybox/source/helpers/jquery.fancybox-buttons.js") ?>"></script>
<script type="text/javascript" src="<?php echo assets_url("fancybox/source/helpers/jquery.fancybox-media.js") ?>"></script>
<script type="text/javascript" src="<?php echo assets_url("fancybox/source/helpers/jquery.fancybox-thumbs.js") ?>"></script>


<script type="text/javascript" src="<?php echo assets_url("jQuery.TosRUs/src/js/jquery.tosrus.min.all.js") ?>"></script>


<!--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.js"></script>-->
<script type="text/javascript" src="<?php echo assets_url('owl-carousel/owl.carousel.js'); ?>"></script>


<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>

<script type="text/javascript" src="<?php echo assets_url("js/jquery.slitslider.js") ?>"></script>

<script type="text/javascript" src="<?php echo assets_url("js/jquery.ba-cond.min.js") ?>"></script>

<!--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.js"></script>-->
<script type="text/javascript" src="<?php echo assets_url('js/wow.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo assets_url('js/imagesloaded.pkgd.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo assets_url('js/masonry.pkgd.min.js'); ?>"></script>


<script type="text/javascript" src="<?php echo assets_url("js/main.js") ?>"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-noty/2.3.8/packaged/jquery.noty.packaged.min.js"></script>

<script src='https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit' async defer></script>

<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery.jssocials/1.1.0/jssocials.min.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.4.1/prism.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.4.1/components/prism-php.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.4.1/components/prism-php-extras.min.js"></script>

<script type="text/javascript">
$(function(){

  var contact_us;


  onloadCallback=function(){

  contact_us=grecaptcha.render(document.getElementById('recaptcha1'), {
          'sitekey' : "<?php echo $recaptcha['key'] ?>",
          'theme' : 'dark'
        });


  }


  $("#kontak-form").on("submit",function(evt){
    evt.preventDefault();    
    if(!$(this)[0].mengirim) {
    __this=$(this)      
    __this[0].mengirim=true;
    var val=__this.serialize();
    var action=__this.attr("action");
    __this.find("button").hide();
    __this.find(".cssload-container").show();
    $.ajax({
      type:"POST",
      url:action,
      data:val,
      cache:false,
      dataType:'json',
      success:function(a){
        if(a.status=='sukses'){
           noty({
            text:"Pesan anda terkirim. Terima kasih telah mengubungi kami",
            type: 'success',
            layout: 'center',            
            dismissQueue:true
                        });
         $('form#kontak-form').find("input[type=text],input[type=password], textarea").val("");
        } else if(a.status=='error') {
          noty({
            text:a.name,
            type: 'error',
            layout: 'center',
            dismissQueue:true

          });
        }

      },
      error:function(){
          noty({
            text:"Cek koneksi internet anda",
            type: 'error',
            layout: 'center',
            dismissQueue:true

          });
      },
      complete:function(){
        grecaptcha.reset(contact_us);  
        __this.find("button").show();
        __this.find(".cssload-container").hide();
        __this[0].mengirim=false;      
      }
    });
  }
  });


 var $grid = $(".grid").masonry({
    itemSelector: '.grid-item',   
    });

 $grid.imagesLoaded().progress( function() {
    $grid.masonry('layout');
 });

$(".grid a").tosrus({
  caption: {
    add:true
  }
});

});

$("#share").jsSocials({
            shares: [ "facebook", "twitter","googleplus", "whatsapp"],
            
 });


<?php echo $informasi['custom_javascript']; ?>

<?php echo $google_analytics["script"]; ?>

</script>




</body>



</html>