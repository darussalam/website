<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<link rel="stylesheet" type="text/css" href="<?php echo assets_url('bootstrap/css/bootstrap.min.css'); ?>" />
<link rel="stylesheet" type="text/css" href="<?php echo assets_url('css/custom.css') ?>" />
<link rel="stylesheet" type="text/css" href="<?php echo assets_url('bootstrap/css/datepicker.css'); ?>" />
<script type="text/javascript" src="<?php echo assets_url('bootstrap/js/jquery.js'); ?>"></script>
<script type="text/javascript" src="<?php echo assets_url('bootstrap/js/bootstrap-datepicker.js'); ?>"></script>
<script type="text/javascript">
    $(document).ready(function()
    {
        $("#tanggal").datepicker(
        {
            format:'dd-mm-yyyy'
        });
    });   
</script>
<div id="main-konten">
    <div class="container" style="padding: 20px">
        <div class="rows col-md-12">
            <div class="col-md-3">
            </div>
            <div class="col-md-6" style="background-color: grey; padding: 0;opacity: 0.6; box-shadow: 1px 1px 50px #000; border-top-left-radius: 40px">
                <center><h3 style="color: white">Formulir Pendaftaran</h3></center>
            </div>
        </div>
        <div class="rows col-md-12">
            <div class="col-md-3">
            </div>
            <div class="col-md-6" style="background-color: white; box-shadow: 1px 1px 100px #000; opacity: 0.7; padding: 10px; overflow: hidden;border-bottom-right-radius: 40px">
                    <form class="form" method="POST" action="">
                        <input style="width: 531px; opacity: 100%" type="text" name="namapendaftar" class="form-control"  placeholder="Nama Lengkap"><br>
                        <input style="width: 531px" type="text" name="tempatlahir" class="form-control"  placeholder="Tempat Lahir"><br>
                        <input style="width: 531px" type="date" name="tanggallahir" class="form-control" placeholder="Tanggal Lahir" id="tanggal"><br>
                        <input style="width: 531px" type="text" name="jeniskelamin" class="form-control"  placeholder="Jenis Kelamin"><br>
                        <input style="width: 531px" type="text" name="alamatpendaftar" class="form-control"  placeholder="Alamat"><br>
                        <input style="width: 531px" type="text" name="tahuntamat" class="form-control"  placeholder="Tahun Lulus"><br>
                        <input style="width: 531px" type="text" name="kontakpendaftar" class="form-control"  placeholder="Kontak"><br>
                        <div class="col-sm-offset-5 btn">
                            <button type="submit" class="btn btn-default">Daftar</button>
                        </div>
                    </form>
            </div>
        </div>
    </div>
</div>