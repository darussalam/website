<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>


	<div class='col-md-12' id="header-page">
			<h2 style='font-size: 25px;color: whitesmoke'><span >BERITA</span></h2>
	</div>


<div class='col-md-8 left-side'>

<div itemscope itemtype="http://schema.org/NewsArticle" class='artikel' style="margin-bottom: 10px;">

	<meta itemscope itemprop="mainEntityOfPage" itemid="<?php echo current_url(); ?>" >

	<div class='konten'>
		<?php 

			echo "<h1 itemprop='headline' class='judul'>$artikel[judul]</h1>";

			 ?>

		<div class='info'>
			<meta itemprop="dateModified" content="<?php 

			if($artikel['tanggal_edit']=='0000-00-00 00:00:00'){
				echo cuma_tanggal($artikel['tanggal']);
			} else {
				echo cuma_tanggal($artikel['tanggal_edit']);

			}

			 ?>"/>
			<?php 


			echo "<span  itemprop='datePublished' content='".cuma_tanggal($artikel['tanggal'])."' class='tanggal'> <i class='fa fa-calendar'></i>&nbsp; ".format_tanggal($artikel['tanggal'])."</span> ";

			 ?>
		</div>
		</div>

<span itemprop="image" itemscope itemtype="https://schema.org/ImageObject" ><img  class="img-responsive" src="<?php echo img_artikel_url($artikel['foto']); ?>" />

<meta itemprop="url" content="<?php echo img_artikel_url($artikel['foto']); ?>">
<meta itemprop="width" content="1000">
<meta itemprop="height" content="600">

</span>

	<div class='konten'>



		<div  itemprop="articleBody" class="isi">

			<?php 
			echo set_tag($artikel['isi']);
			 ?>

		</div>

		<div id="share" style="margin:50px 0 50px 0;"></div>

	</div>

</div>


</div>