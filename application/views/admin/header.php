<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
function path_adm(){echo base_url()."an-theme/admin";}
function rpath_adm(){return base_url()."an-theme/admin";}

$name=$this->session->userdata('name_user');
$levela=($user_level=='1')?"Super Admin":"Admin";


?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
        <title>Pondok Pesantren Darussalam</title>
        <link rel="stylesheet" type="text/css" href="../an-theme/ando/assets/css/main.css">
        <link rel='shortcut icon' href='an-component/media/upload-galeri/logo.png' /> 
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <link href="<?php path_adm() ?>/dist/dropzone/dropzone.min.css" rel="stylesheet" type="text/css" />
        <!-- Bootstrap 3.3.4 -->
        <link href="<?php path_adm() ?>/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- Font Awesome Icons -->
        <link href="<?php path_adm() ?>/plugins/font-awesome-4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php path_adm() ?>/plugins/ionicons/css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="<?php path_adm() ?>/dist/css/AdminLTE.css" rel="stylesheet" type="text/css" />
        <!-- DATATABLES -->
        <link href="<?php path_adm() ?>/plugins/datatables/media/css/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
        <link href="<?php path_adm() ?>/plugins/datatables/extensions/Responsive/css/responsive.dataTables.css" rel="stylesheet" type="text/css" />
        <!-- DATATABLES -->
        <link href="<?php path_adm() ?>/dist/css/skins/skin-red.css" rel="stylesheet" type="text/css" />
        <link href="<?php path_adm() ?>/dist/codemirror/lib/codemirror.css" rel="stylesheet" type="text/css" />
        <link href="<?php path_adm() ?>/dist/codemirror/theme/3024-day.css" rel="stylesheet" type="text/css" />
        <link href="<?php path_adm() ?>/dist/css/ando_admin.css" rel="stylesheet" type="text/css" />
 <?php if($npage==10){ ?>
 <link href='<?php path_adm()?>/plugins/jquery-ui-1.11.4/jquery-ui.min.css' rel='stylesheet' type='text/css'> 
<?php
 } 

 ?>
 
  
   

    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  
  <body class="skin-red sidebar-mini">


 <div class="wrapper">

      <!-- Main Header -->
      <header class="main-header">
          <div class="user-panel logo"><!-- Sidebar user panel (optional) -->
                        <div class="pull-left image">
                            <img src="<?php echo $path_avatar; ?>" class="img-circle" alt="User Image" />
                        </div>
                        <div class="pull-left info">
                        </div>
                    </div>

        <!-- Header Navbar -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <h3 class="navbar-brand">Pondok Pesantren Darussalam</h3>
          <!-- Navbar Right Menu -->
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
             <!-- <li class='status_koneksi'><a href='#' class='koneksi_stat'></a></li>-->
        <!-- User Account Menu -->
              <li class="dropdown user user-menu">
                <!-- Menu Toggle Button -->
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <!-- The user image in the navbar-->
                  <span class="hidden-xs"><?php echo $user; ?></span>
                </a>
                <ul class="dropdown-menu">
                  <!-- The user image in the menu -->
                  <li class="user-header">
                    <img src="<?php echo $path_avatar; ?>" class="img-circle" alt="User Image" />
                    <p>
                      <?php echo $name ?> <br> <?php echo $levela ?>
                      <small></small>
                    </p>
                  </li>
                  <!-- Menu Body -->
                  <li class="user-body">
                    <div class="col-xs-4 text-center">
                      <a href="#"> </a>
                    </div>
                    <div class="col-xs-4 text-center">
                      <a href="#"> </a>
                    </div>
                    <div class="col-xs-4 text-center">
                      <a href="#"> </a>
                    </div>
                  </li>
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="<?php echo base_url() ?>admin/profil_user" class="btn btn-default btn-flat">Profil</a>
                    </div>
                    <div class="pull-right">
                      <a href="<?php echo base_url() ?>admin/logout" class="btn btn-default btn-flat">Keluar</a>
                    </div>
                  </li>
                </ul>
              </li>

            </ul>
          </div> <!--End .navbar-custom-menu -->
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
       
          <ul class="sidebar-menu">


            <li class="header">Navigasi</li>
            <li class='<?php if($npage==1){ echo'active';} ?>'><a href="<?php echo $burl; ?>"><i class='fa fa-dashboard'></i> <span>Halaman Utama</span></a> </li>
            
            <li class='treeview  <?php if(($npage==6) OR( $npage==7)){ echo'active';} ?>'>
              <a href='#'><i class='fa fa-pencil-square'></i><span>Berita</span><i class="fa fa-angle-left pull-right"></i></a>
              <ul class='treeview-menu'>
                <li class='<?php if($npage==7){ echo'active';} ?>'><a href='<?php echo $burl; ?>/all_artikel'><i class='fa fa-circle-o'></i><span>Semua Berita</span></a></li>
                <li class='<?php if($npage==6){ echo'active';} ?>'><a href='<?php echo $burl; ?>/artikel'><i class='fa fa-circle-o'></i><span>Tambah Berita</span></a></li>
              </ul>
            </li>

            <li class='header'>Pengaturan</li>

           <?php if($user_level=='1'){    ?>
             
               <li class='<?php if($npage==17 || $npage==16){ echo'active';} ?>'><a href='<?php echo $burl; ?>/all_menu'><i class='fa fa-th-list'></i><span>Pengaturan Menu</span></a></li>
             
                <?php }                
               ?>      

            <li class='treeview <?php if(($npage==3) OR( $npage==4) OR ($npage==5)){ echo'active';} ?>'>
              <a href="#"><i class='fa fa-user'></i><span>Pengguna</span><i class="fa fa-angle-left pull-right"></i></a>
              <ul class='treeview-menu'>
                <?php if($user_level=='1'){
                  ?>
               <li class='<?php if($npage==4){ echo'active';} ?>'><a href='<?php echo $burl; ?>/all_user'><i class='fa fa-circle-o'></i><span>Manajemen Pengguna</span></a></li>
               <li class='<?php if($npage==3){ echo'active';} ?>'><a href='<?php echo $burl; ?>/user_baru'><i class='fa fa-circle-o'></i><span>Pengguna Baru</span></a></li>
                <?php }
               ?>               
              </ul>
            </li>

            <li class='treeview <?php if($npage==10 OR $npage==11 OR $npage==20 OR $npage==22){ echo'active';} ?>'>
              <a href="#"><i class='fa fa-toggle-on'></i><span>Pengaturan Halaman</span><i class="fa fa-angle-left pull-right"></i></a>
              <ul class='treeview-menu'>
               <li class='<?php if($npage==10){ echo'active';} ?>'><a href='<?php echo $burl; ?>/informasi'><i class='fa fa-circle-o'></i><span>Informasi Halaman</span></a></li>
              <li class='<?php if($npage==20){ echo'active';} ?>'><a href='<?php echo $burl; ?>/banner_depan'><i class='fa fa-circle-o'></i><span>Banner Depan</span></a></li>
              </ul>
            </li>


          </ul><!-- /.sidebar-menu -->
          <br><br>
        </section>
        <!-- /.sidebar -->
      </aside>

