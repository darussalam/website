<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

     <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper" >
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <i class="fa fa-dashboard"></i> Halaman Utama
          </h1>
        </section>

        <!-- Main content -->
        <section class="content">

         

        <div class="col-md-12">
            <div class="col-lg-4 col-xs-6">
              <div class="small-box bg-aqua" style="height: 160px">
                <div class="inner">
                  <h3><?php echo $data['jumlah_artikel']; ?></h3>
                  <p>Berita Tersimpan</p>
                </div>
                <div class="icon" style="padding-top: 30px">
                  <i class="ion ion-edit"></i>
                </div><br>
                <a href="<?php echo base_url('admin/all_artikel') ?>" class="small-box-footer">Selengkapnya <i class="fa fa-arrow-circle-right"></i></a>
              </div>

              <div class="small-box bg-green" style="height: 160px">
                <div class="inner">
                  <h3><?php echo $data['jumlah_halaman']; ?></h3>
                  <p>Jumlah Pendaftar</p>
                </div>
                <div class="icon" style="padding-top: 30px">
                  <i class="ion ion-person"></i>
                </div><br>
                <a href="<?php echo base_url('admin/all_page') ?>" class="small-box-footer">Selengkapnya <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div>

            <div class="col-md-8 col-sm-6 col-xs-12">
              <div class="box box-info">
                <div class="box-header with-border">
                  Berita Paling Baru
                </div>
                <div class="box-body">

                  <table class="table table-striped">
                    <?php 

                      foreach ($data["artikel_terbaru"] as $artikel ) {
                     $label=$artikel['artikel_status']=='draft'?"label-warning":"label-primary";
                     echo "<tr><td style='font-family: \"Source Sans Pro\" ;'><a style='color:#000; ' href='".base_url("admin/artikel/$artikel[artikel_id]")."'><strong>".character_limiter($artikel['artikel_judul'],75)."</strong></a>

                     <br><span class='label pull-right $label'>$artikel[artikel_status]</span>

                     </td></tr>";
                  }

                   ?>
                  </table>
                </div>
              </div>
            </div>
          </div>

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

   