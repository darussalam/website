<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
?>


     <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper" >
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Daftar Berita
          </h1>
        </section>

        <!-- Main content -->
        <section class="content">

         		<div class="box box-warning">
				<div class="box-body">

					<table class="slug-table table table-bordered table-striped table-responsive dt-responsive" cellspacing="0" width="100%">
						<thead>
						<tr>
							<th>Judul</th>
							<th>Status</th>
							<th>Ditambahkan</th>
							<th>Terakhir Diubah</th>
							<th>Aksi</th>
						</tr>
						</thead>

						<tbody>
							<?php
							echo $artikels;
							?>
						</tbody>
					</table>


				</div>
				</div>

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

