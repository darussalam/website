<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
?>

<div class="content-wrapper">
	<section class="content-header">
		<h1><?php echo ($modul=='edit')?"Ubah Berita":"Berita Baru" ?> </h1>
	</section>

	<section class="content">
		<div class="box box-warning">
		<div class="box-body">
				<div class="row">
					<div class="col-md-12 col-xs-12">

						<div class="form-group">
							<label for="judul_artikel">Judul Berita</label>
							<input type="text" class="form-control" name="judul_artikel" id="judul_artikel" value='<?php echo $artikel_judul ?>' >
						</div>

						<div class="form-group">
							<label for="isi_artikel">Isi Berita</label>
							<textarea  id='isi_artikel' class='isi_artikel form-control'><?php echo $artikel_isi ?></textarea>
						</div>

						<div class="form-group">
                   		 <input type='hidden' class='sesi-from_artikel' value='<?php echo rand(0,100).rand(10,500).date('dym') ?>' >
                   		 <input type='hidden' class='id_artikel' value='<?php echo $artikel_id ?>' >
						</div>

						<div class="form-group">
							<label>Foto</label> <small>(bisa lebih dari satu)</small>
							<div class="dropzone well well-sm foto_gallery_artikel">
							</div>
						</div>

						<div class="form-group" style="height:30px">
							<button class='btn btn-sm btn-danger tbl-hapus-multi'><i class='glyphicon glyphicon-trash'></i>  Hapus Foto Terpilih </button>
						 </div>

						<div class="form-group"><div class="row foto-artikel-area"><?php 
						if($artikel_photos!=false){
							foreach ($artikel_photos as  $value) {
								$featured=($value["featured"]=="Y")?"<span class='label label-primary featured-label'>Featured</span>":"";
								$ftrue=($value["featured"]=="Y")?"featured-true":"";
								echo "
								<div class='aPhotoThumb col-md-4 col-xs-4 $ftrue'>
								$featured
								<span class='label label-danger hapus_label'> menghapus... </span>
								<button class='btn btn-warning btn-sm featured-tombol'>jadikan featured image</button>
								<span class='glyphicon glyphicon-remove hapus_foto_artikel' id='$value[id_foto]'></span>
								
								<div class='hover_foto_artikel' ></div>";
								echo "<img src='$path_art_photo_thumb/$value[nama_foto]'>";
								echo "</div>";
							}
						}
						?></div></div>

						<?php
						if($artikel_id_user==false OR $artikel_id_user==$id_user OR $user_level==1){
						echo '<div class="form-group">
							<label>Izinkan pengguna lain untuk mengubah berita</label>
							<div class="iz_edit">YES</div>
						</div>';
						}
						?>

						</div>

						<div class="form-group">
							

							 <button class="btn btn-sm btn-primary save-artikel"><?php echo ($artikel_id==0 || $artikel_status=='draft')?'Sebarkan':'Perbaharui' ?></button>
							
							&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 

							<button class="btn btn-xs  draft-artikel"><?php echo ($artikel_status=='publish')?"Kembalikan ke draft":"Simpan di draft"?></button> <small class='time-draft'></small><small class='pesan-draft'></small> 

						</div>
					</div>
				</div>
			<div class="box-body">
			</div>
		</div>
		</div>


	</section>
</div>